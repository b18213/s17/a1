/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	function getPersonalInfo(){
		let full_name = prompt("Enter Full name:");
		let age = prompt("Enter Age");
		let location = prompt("Enter Location");
		alert ("Information received");

		console.log("Your name is: " + full_name);
		console.log("Your age is: " + age);
		console.log("Your location is in: " + location);
	};

	getPersonalInfo();


/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function getMusicians(){
		let musician_list = ["Pierce the Veil", "Sleeping with Sirens", "Linkin Park", "Eminem", "Gloc-9"];
		console.log("Your favorite musicians are:")
		console.log(musician_list);
	};

	getMusicians();
/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function getMovies(){
		let movie_1 = "1. John Wick\n rotten tomato rating: 86 ";
		let movie_2 = "2. Violet Evergarden: The Movie\n rotten tomato rating: 100";
		let movie_3 = "3. The Disappearnce of Haruhi Suzumiya\n rotten tomato rating: 94";
		let movie_4 = "4. Logan\n rotten tomato rating: 94";
		let movie_5 = "5. Captain America: Civil War\n rotten tomato rating: 90";

		console.log(movie_1);
		console.log(movie_2);
		console.log(movie_3);
		console.log(movie_4);
		console.log(movie_5);
	}

	getMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

/*printUsers();*/
let printFriends/*()*/ = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();


// console.log(friend1);
// console.log(friend2);